const data = require('./drill_data.cjs');


// Very interesting way of using sort to account for case-sensitive sorting.
// For understanding in case you forget refer to https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
// Look at compareFn description
const modelSort = objectData => {
    if(!Array.isArray(objectData) || !objectData.length) {
        return([]);
    }
    const sortedCars = [];
    const maxLength = objectData.length;
    for(let currentIndex = 0; currentIndex < maxLength; currentIndex++){
        sortedCars.push(objectData[currentIndex]['car_model']);
    }
    return(sortedCars.sort(function (w1,w2){
        w1 = w1.toLowerCase();
        w2 = w2.toLowerCase();
        if(w1 === w2) {
            return(0);
        } else if (w1 > w2) {
            return(1);
        }
        return(-1);
    }));
}

const final = modelSort(data);

console.log(final);

module.exports = modelSort;