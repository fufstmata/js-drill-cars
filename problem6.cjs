const data = require('./drill_data.cjs');

const Sauber = objectData => {
    if(!Array.isArray(objectData) || !objectData.length) {
        return([]);
    }
    const maxLength = objectData.length;
    const collection = [];
    for(let currentIndex = 0; currentIndex < maxLength; currentIndex++) {
        let currentCar = objectData[currentIndex]['car_make']
        if(currentCar === 'Audi' || currentCar === 'BMW') {
            collection.push(objectData[currentIndex]);
        }
    }
    return(collection);
}

const BMWAndAudi = Sauber(data);
console.log(JSON.stringify(BMWAndAudi));

module.exports = Sauber;