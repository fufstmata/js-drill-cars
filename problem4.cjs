const data = require('./drill_data.cjs');

const yearRetrieve = objectData => {
    if(!Array.isArray(objectData) || !objectData.length) {
        return([]);
    }
    const yearData = [];
    const maxLength = objectData.length;
    for(let currentIndex = 0; currentIndex < maxLength; currentIndex++){
        yearData.push(objectData[currentIndex]['car_year']);
    }
    return(yearData);
}

const final = yearRetrieve(data);

console.log(final);

module.exports = yearRetrieve;