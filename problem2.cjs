const data = require('./drill_data.cjs');

const last = objectData => {
    if(!Array.isArray(objectData) || !objectData.length) {
        return([]);
    }
    return(objectData[objectData.length - 1]);
}    
    
const final = last(data);

console.log(`Last car is ${final['car_make']} ${final['car_model']}`);

module.exports = last;