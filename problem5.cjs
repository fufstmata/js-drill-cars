const yearFunction = require('./problem4.cjs');
const data = require('./drill_data.cjs');


const yearData = yearFunction(data);

// Could have done with simply using count but I wanted to edit with filter logic for practice
const sortYears = (objectData) => {
    if(!Array.isArray(objectData)){
        return(0);
    } else {
        const test = objectData.filter(year => {
        return(year > 2000);
    }) 
        return(test.length);
    }
}

const sortedYears = sortYears(yearData);
module.exports = sortYears;