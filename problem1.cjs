const data = require('./drill_data.cjs');


// Arrow function definition stored in variable locate
// Very important to perform all pre-requisite checks before executing code

const locate = (objectData,idNumber) => {
    if(!Array.isArray(objectData) || !objectData.length || !Number.isFinite(idNumber)){
        return([]);
    }
    const maxLength = objectData.length;
    for(let currentIndex = 0; currentIndex < maxLength; currentIndex++){
        if(objectData[currentIndex].id === idNumber) {
            return(objectData[currentIndex]);
        }
    }
};

const product = locate(data,33);
console.log(`Car 33 is a ${product['car_year']} ${product['car_make']} ${product['car_model']}`);

module.exports = locate;


// LOT of different ways to approach this such as

// const locate = ((objectData,idNumber) => {
//     maxLength = objectData.length;
//     for(let currentIndex = 0; currentIndex < maxLength; currentIndex++){
//         if(objectData[currentIndex].id === idNumber) {
//             console.log(objectData[currentIndex]);
//         }
//     }
// })(data,33);
