const test_func  = require('../problem2.cjs');
const data = require('../drill_data.cjs');
const assert = require('assert');

const test_data = test_func(data);

const expected = { id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 };

assert.deepStrictEqual(test_data,expected);
